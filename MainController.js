//MainController.js

    // create the module and name it app
var uid=1;
var app = angular.module('hello',['ngRoute']);

   // create the controller and inject Angular's $scope
   app.config(['$routeProvider',function($routeProvider){
   $routeProvider
	.when("/home",{
	templateUrl:"Home.html",
	controller:"HomeController"
}).
when("/login",{
templateUrl: "LoginView.html",
controller: 'LoginController'
}).
otherwise({redirectTo:'/index.html'});
}]);
   

app.controller('MainController', function($scope) {

        // create a message to display in our view
        $scope.message = 'Everyone come and see how good I look!';
    });


app.controller('HomeController', function($scope) {
     
    $scope.message = 'This is Home Screen';
     
});

app.controller('LoginController', function($scope) {

$scope.usersdata = [
        { id:0,
          'email':'hello@gmail.com', 
          'password': '12345'
        }
    ];
     
    $scope.saveUser = function() {
         
        if($scope.newuser.id == null) {
        //if this is new user, add it in usersdata array
        $scope.newuser.id = uid++;
        $scope.usersdata.push($scope.newuser);
        } else {
        //for existing user, find this user using id
        //and update it.
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == $scope.newuser.id) {
            $scope.usersdata[i] = $scope.newuser;
            }
        }                
        }
         
        //clear the add user form
        $scope.newuser = {};
    }
 // delete function    
    $scope.delete = function(id) {
         
        //search user with given id and delete it
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == id) {
                $scope.usersdata.splice(i,1);
                $scope.newuser = {};
            }
        }
         
    }
     // edit function 
    $scope.edit = function(id) {
    //search  with given id and update it
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == id) {
                //we use angular.copy() method to create 
                //copy of original object
                $scope.newuser = angular.copy($scope.usersdata[i]);
            }
        }
    
}
     
});
 



