var uid = 1;
var app= angular.module("app",[])
app.controller("UserController",function($scope){
$scope.usersdata = [
        { id:0,
          'email':'hello@gmail.com', 
          'password': '12345'
        }
    ];
     
    $scope.saveUser = function() {
         
        if($scope.newuser.id == null) {
        //if this is new user, add it in usersdata array
        $scope.newuser.id = uid++;
        $scope.usersdata.push($scope.newuser);
        } else {
        //for existing user, find this user using id
        //and update it.
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == $scope.newuser.id) {
            $scope.usersdata[i] = $scope.newuser;
            }
        }                
        }
         
        //clear the add user form
        $scope.newuser = {};
    }
 
     
    $scope.delete = function(id) {
         
        //search user with given id and delete it
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == id) {
                $scope.usersdata.splice(i,1);
                $scope.newuser = {};
            }
        }
         
    }
     
     
    $scope.edit = function(id) {
    //search  with given id and update it
        for(i in $scope.usersdata) {
            if($scope.usersdata[i].id == id) {
                //we use angular.copy() method to create 
                //copy of original object
                $scope.newuser = angular.copy($scope.usersdata[i]);
            }
        }
    }});
